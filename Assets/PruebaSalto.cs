﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaSalto : MonoBehaviour {

    bool jump = true;
    string direction;
    Rigidbody2D rb2D;
    float speed;

	// Use this for initialization
	void Start () {
        rb2D = this.GetComponent<Rigidbody2D>();
        speed = 10;

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space) && jump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
            jump = false;
        }
        //horizontal move
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            direction = "left";
            rb2D.AddForce(Vector3.left * speed);


        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            direction = "right";
            rb2D.AddForce(Vector3.right * speed);



        }


        else
        {
            rb2D.AddForce(Vector3.zero);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "grounded") {
            jump = true;
            Debug.Log("Now");
        }
    }
}
