﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour {

    private Rigidbody2D rb2D;
    private GameObject Camara;
    private GameObject otherPlayer; 

    private float move;
    public float speed;
    public float jumpSpeed;
    public float whiteX;
    public float whiteY;
    private float countJump;

    private bool jump;
    private bool grounded;
    private bool death;
    public bool white;
    public bool collisionWhite;
    public bool collisionRed;
    public bool pressed;

    private string direction;
    private string directionCollition;

    // Use this for initialization
    void Start()
    {
        Camara = GameObject.FindGameObjectWithTag("MainCamera");
        otherPlayer = GameObject.FindGameObjectWithTag("red");
        rb2D = GetComponent<Rigidbody2D>();
        move = 0.0f;
        speed = 7;
        jumpSpeed = 6.5f;
        countJump = 0;
        collisionWhite = false;
        collisionRed = false;
        death = false;
        pressed = false;
        startPosition();

    }

    // Update is called once per frame
    void Update(){
             
        //Salir de la camara
        if (Camara.transform.position.x + 12.5f < transform.position.x || Camara.transform.position.x - 12.5f > transform.position.x){
            restartPosition();
        }
        else{
            //jump = true;
        }


        //Jump
        if (Input.GetKeyDown(KeyCode.Space) && GetComponent<Rigidbody2D>().velocity.y == 0){
            rb2D.AddForce(Vector3.up * jumpSpeed, ForceMode2D.Impulse);
            jump = false;
        }
                
        //Restart level
        if (Input.GetKeyDown(KeyCode.R)){
            restartPosition();
        }
    }

    void FixedUpdate()
    {
        //horizontal move
        if (Input.GetKey(KeyCode.LeftArrow)){
            if (GetComponent<Rigidbody2D>().velocity.y == 0){
                rb2D.AddForce(Vector3.left * speed);
                countJump = 0;
                
            }
            else if (countJump < 5){
                rb2D.AddForce(Vector3.left * speed);
                countJump++;
            }

        }
        else if (Input.GetKey(KeyCode.RightArrow)){
            if (GetComponent<Rigidbody2D>().velocity.y == 0){
                rb2D.AddForce(Vector3.right * speed);
                countJump = 0;
               
            }
            else if (countJump < 5){
                rb2D.AddForce(Vector3.right * speed);
                countJump++;
            }
        }

        else{
            rb2D.AddForce(Vector3.zero);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {

        /*else if(col.gameObject.tag == "white" || col.gameObject.tag == "red"){
             jump = true;
             jumpSpeed = 2.5f;
         }*/
        if (col.gameObject.tag == "death") {

            restartPosition();
        }
        if (col.gameObject.tag == "grounded") {
            //Debug.Log("Now");
            jump = true;
        }

        if (col.gameObject.tag == "MovingPlatform") {
            transform.parent = col.transform;
            jump = true;

        }

    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.tag == "MovingPlatform")
        {

            transform.parent = null;
        }
    }

    private void startPosition() {
        whiteX = this.GetComponent<Transform>().position.x;
        whiteY = this.GetComponent<Transform>().position.y;
    }

    private void restartPosition(){
        this.GetComponent<Transform>().position = new Vector3(whiteX, whiteY, transform.position.z);
        //this.GetComponent<SpriteRenderer>().enabled = true;
        otherPlayer.SetActive(true);

        /*otherplayer.GetComponent<Transform>().position = new Vector3(otherplayer.GetComponent<playerMovementRed>().redX, otherplayer.GetComponent<playerMovementRed>().redY, otherplayer.GetComponent<playerMovement>().transform.position.z);
        otherplayer.GetComponent<SpriteRenderer>().enabled = true;*/

    }

}
