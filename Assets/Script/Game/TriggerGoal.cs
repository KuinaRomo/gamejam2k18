﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerGoal : MonoBehaviour {


    public bool forRed = false;
    public bool forWhite = false;

    public bool WhiteDoneGoal;
    public bool RedDoneGoal;



    // Use this for initialization
    void Start () {
        WhiteDoneGoal = false;
        RedDoneGoal = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        playerMovement playercollidedwhite = col.gameObject.GetComponent<playerMovement>();
        playerMovementRed playercollidedRed = col.gameObject.GetComponent<playerMovementRed>();

        if (playercollidedwhite && forWhite)
        {
     
            WhiteDoneGoal = true;
            GameObject.Find("Main Camera").GetComponent<FollowChapter>().followObject = GameObject.FindGameObjectWithTag("red");
            playercollidedwhite.gameObject.SetActive(false);

            //gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
        else if(playercollidedRed && forRed)
        {

            RedDoneGoal = true;
            playercollidedRed.gameObject.SetActive(false);
            //gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }


    }



}
