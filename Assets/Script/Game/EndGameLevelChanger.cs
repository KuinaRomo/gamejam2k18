﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameLevelChanger : MonoBehaviour
{

    public string LevelName;
    public int actualLevelPos;
    public TriggerGoal[] Goals;

    public AudioSource aSource;
    private AudioClip aClip;

   

    // Use this for initialization
    void Start()
    {
        aClip = GetComponent<AudioClip>();
        Sound();
    }

    void Sound()
    {
        int numberSound = Random.Range(0, 3);

        switch (numberSound)
        {
            case 0:
                aClip = (AudioClip)Resources.Load("music game1", typeof(AudioClip));
                aSource.clip = aClip;
                aSource.Play();
                break;
            case 1:
                aClip = (AudioClip)Resources.Load("music game2", typeof(AudioClip));
                aSource.clip = aClip;
                aSource.Play();
                break;
            case 2:
                aClip = (AudioClip)Resources.Load("music game3", typeof(AudioClip));
                aSource.clip = aClip;
                aSource.Play();
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

        Goals = FindObjectsOfType<TriggerGoal>();
        int counter = 0;

        //Debug.Log(Goals.Length);
        for (int i = 0; i < Goals.Length; i++)
        {

            if (Goals[i].WhiteDoneGoal || Goals[i].RedDoneGoal)
            {
                counter++;
            }
        }

        if (counter >= 2)
        {

            SceneManager.LoadScene(actualLevelPos+1);

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }

    }
}