﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {


    public GameObject platform;
    public float moveSpeed;

    private Transform currentPoint;
    public Transform[] Points;
    public int pointSelection;
    private float TempWaiting;
    private int goingBackNum;


    // Use this for initialization
    void Start () {
        currentPoint = Points[pointSelection];
        TempWaiting = 1;
        goingBackNum = 1;
    }

    // Update is called once per frame
    void Update() {
        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);


        if (platform.transform.position == currentPoint.position)
        {

            TempWaiting -= Time.deltaTime;

            if (TempWaiting < 0)
            {
                TempWaiting = 1;
                pointSelection += 1 * goingBackNum;
                if (pointSelection == Points.Length)
                {
                    pointSelection = Points.Length - 1;
                    goingBackNum *= -1;
                }
                else if(pointSelection < 0){

                    pointSelection = 0;
                    goingBackNum *= -1;

                }
                
                currentPoint = Points[pointSelection];
            }
        }

        
	}
}
