﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowChapter : MonoBehaviour {

    private GameObject redSquare;
    private GameObject whiteSquare;
    public GameObject followObject;

	// Use this for initialization
	void Start () {
        redSquare = GameObject.FindGameObjectWithTag("red");
        whiteSquare = GameObject.FindGameObjectWithTag("white");
        followObject = GameObject.FindGameObjectWithTag("white");
    }
	
	// Update is called once per frame
	void Update () {
        /*if (whiteSquare.GetComponent<playerMovement>().collisionWhite) {
            followObject = redSquare;
        }
        else if(redSquare.GetComponent<playerMovement>().collisionRed){
            followObject = whiteSquare;
        }
        else{
            transform.position = new Vector3(followObject.transform.position.x, transform.position.y, transform.position.z);
        }*/
        transform.position = new Vector3(followObject.transform.position.x, followObject.transform.position.y, transform.position.z);

        //Choque de personaje con uno de los extremos de la camara

    }
}
