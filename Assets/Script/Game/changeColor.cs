﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeColor : MonoBehaviour {

    private SpriteRenderer sR;
    private Color redsalmon = new Color(255 / 255F, 30 / 255F, 30 / 255F, 1);


	// Use this for initialization
	void Start () {
        sR = GetComponent<SpriteRenderer>();
        sR.color = redsalmon;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q)){
            if(sR.color == redsalmon){
                sR.color = Color.white;
            }
            else if(sR.color == Color.white){
                sR.color = redsalmon;
            }
        }
	}
}
