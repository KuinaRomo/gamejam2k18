﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class testButton : MonoBehaviour {

    public AudioSource aSource;
    private int NumeroDeEscena;

    void Start ()
    {
        aSource = GetComponent<AudioSource>();
    }

   public void PlayScene()
    {
        NumeroDeEscena = 0;
        sound();
        StartCoroutine("loadSceneAfterDelay");
    }
 
    public void QuitGame()
    {
        NumeroDeEscena = 1;
        sound();
        StartCoroutine("loadSceneAfterDelay");
    }

    public void GoToCredits()
    {
        NumeroDeEscena = 2;
        sound();
        StartCoroutine("loadSceneAfterDelay");

    }

    public void sound()
    {
        aSource.Play();
    }

    public IEnumerator loadSceneAfterDelay()
    {
        yield return new WaitForSeconds(aSource.clip.length);
        if (NumeroDeEscena == 0)
        {
            SceneManager.LoadScene("Level 1 - Tutorial");
        }
        if(NumeroDeEscena == 1)
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
		        Application.Quit();
            #endif
        }

        if(NumeroDeEscena == 2)
        {
            SceneManager.LoadScene("TestCredits");
        }
    }
}
